<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa;

class Nordea
{
    // Sequence Codes
    private static $redirectAuth;
    private static $decoupleAuth;
    private static $ais;
    private static $pisDomestic;
    private static $pisOwnTransfer;

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();

        $sequences = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Nordea();

        self::$redirectAuth = $sequences->Nordea_Redirect_Auth();
        self::$decoupleAuth = $sequences->Nordea_Decouple_Auth();
        self::$ais = $sequences->Nordea_AIS();
        self::$pisDomestic = $sequences->Nordea_PIS_Domestic();
        self::$pisOwnTransfer = $sequences->Nordea_PIS_OwnTransfer();
    }

    private static function getSequenceCode($name) {
      if(self::$$name) {
        return self::$$name;
      }
      else {
        self::selfConstruct();
        return self::getSequenceCode($name);
      }
    }

    private static function executeSequenceMethod($code, $methodIndex, $alterations, $name, $returnPayload)
    {
        $sequence = Finpaa::getSequenceMethods($code);

        if (isset($sequence->SequenceExecutions)) {

          if($methodIndex < count($sequence->SequenceExecutions))
          {
            $response = Finpaa::executeTheMethod(
              $sequence->SequenceExecutions[$methodIndex]->methodCode, $alterations, $returnPayload
            );

            return array('error' => false, 'response' => json_decode(json_encode($response), true));
          }
          else {
              return array('error' => true, 'message' => $name . ' method Failed -> Index out of bound',
                'methodIndex' => $methodIndex, 'methodsCount' => count($sequence->SequenceExecutions)
              );
          }
        }
        else {
          return array('error' => true,
            'message' => $name .' -> No sequence executions', 'response' => json_decode(json_encode($sequence), true)
          );
       }
    }

    public static function redirectAuthorize($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('redirectAuth'), $methodIndex, $alterations, 'Nordea Redirect Auth', $returnPayload
        );
    }
    public static function decoupleAuthorize($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('decoupleAuth'), $methodIndex, $alterations, 'Nordea Decouple Auth', $returnPayload
        );
    }
    public static function AIS($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('ais'), $methodIndex, $alterations, 'Nordea AIS', $returnPayload
        );
    }
    public static function PisDomestic($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('pisDomestic'), $methodIndex, $alterations, 'Nordea PIS Domestic', $returnPayload
        );
    }
    public static function PisOwnTransfer($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('pisOwnTransfer'), $methodIndex, $alterations, 'Nordea PIS Own Transfer', $returnPayload
        );
    }
}
